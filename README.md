## Instruções
Comandos talvez necessitem ser rodados com `sudo`

Para instalar os pacotes e dependencias utilizados no projeto,
dentro do endereço `/enfase-rebuild`:
### `yarn install`

## Backend

Para instalar Prisma CLI, rode os seguintes comandos:
### `brew tap prisma/prisma`
### `brew install prisma`

Para conectar Prisma ao banco de dados utilizando Docker,
estando no endereço `/enfase-rebuild/server/prisma`:
### `docker-compose up`

Para dar deploy no modelo de banco de dados criado:
### `prisma deploy`
### `prisma generate`

Prisma estará conectado e rodando em `http://localhost:4467` dando acesso
para ver e alterar dados adicionando `/_admin` ao endereço acima.

Agora no endereço `/enfase-rebuild/server`
Para rodar e conectar o servidor ao banco de dados:
### `node src/index.js`

O servidor estará localizado no endereço `http://localhost:4000`

## Frontend

No endereço `/enfase-rebuild`, para rodar a aplicação:
### `yarn start`

A aplicação estará rodando em `http://localhost:3000`

## Comentarios Gerais

A aplicação foi desenvolvida utilizando as seguintes tecnologias:
Prisma(MySQL), GraphQL, Apollo, React e Material-UI.

O esquema planejado para o banco de dados retrata a situação em que uma questão `Question` possui uma descrição
e uma lista de respostas `[Answer!]`. Uma resposta `Answer` possui uma descrição, pertence a uma questão `Question`
e pode ser uma resposta correta ou não. Uma questão pode conter mais de uma respostas correta dentro de si.

Nessa mesma aplicação, o usuário(não precisa estar autenticado) pode ver, adicionar, alterar e remover questões,
as quais possuem uma descrição e respostas dentro de si. O usuário também pode ver e adicionar novas respostas
dentro de uma questão. Tinha-se como objetivo implementar a alteração e remoção de respostas também, no entanto,
essas funcionalidades não foram finalizadas.

Ao ter acesso à aplicação, o usuário pode adicionar novas questões clicando no `floating action button` presente
no canto inferior direito da página. Após adicionar uma questão, é possivel acessá-la através dos botões `view`,
`edit` e `delete`. Estes botões permitem a visualização, alteração e remoção da questão respectivamente.
Para adicionar uma respostas a uma questão, o usuário deve acessar a questão através do botão `edit` e selecionar
`create answer`.

Não foi implementada a funcionalidade de atualizar a página quando alterações fossem feitas. Portanto, é necessário
dar refresh na página para sincronizar ações feitas na aplicação.



