import React from 'react'
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const DELETEQUESTION_MUTATION = gql`
  mutation DeleteQuestionMutation($id: ID!) {
    deleteQuestion(id: $id) {
      id
    }
  }
`

function DeleteQuestionButton(props) {
  const [deleteQuestion, { data }] = useMutation(DELETEQUESTION_MUTATION)

  return (
    <Button
      onClick={e => {
        e.preventDefault()
        deleteQuestion({ variables: {id: props.question.id} })
        props.onClick()
      }}
      color="primary"
    >  
      Yes
    </Button>
  ) 
}

export default DeleteQuestionButton
