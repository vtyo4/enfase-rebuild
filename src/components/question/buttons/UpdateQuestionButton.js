import React from 'react'
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const UPDATEQUESTION_MUTATION = gql`
  mutation UpdateQuestionMutation($input: QuestionUpdateInput!) {
    updateQuestion(input: $input) {
      id
    }
  }
`

function UpdateQuestionButton(props) {
  const [updateQuestion, { data }] = useMutation(UPDATEQUESTION_MUTATION)

  return (
    <Button
      onClick={e => {
        e.preventDefault()
        updateQuestion({
          variables: {
            input: {
              id: props.questionId,
              description: props.newInput.description
            }
          }
        })
      }}
      color="primary"
    >  
      Save Changes
    </Button>
  ) 
}

export default UpdateQuestionButton
