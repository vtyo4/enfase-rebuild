import React from 'react'
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const CREATEQUESTION_MUTATION = gql`
  mutation CreateQuestionMutation($input: QuestionCreateInput!) {
    createQuestion(input: $input) {
      id
      description
    }
  }
`

function CreateQuestion(props) {
  const [createQuestion, { data }] = useMutation(CREATEQUESTION_MUTATION)

  return (
    <Button
      onClick={e => {
        e.preventDefault()
        createQuestion({
          variables: {
            input: {
              description: props.input.description
            }
          }
        })
        props.onClick()
      }}
      color="primary"
    >  
      Create Question
    </Button>
  ) 
  
}

export default CreateQuestion
