import React from 'react'
import CreateQuestionButton from '../buttons/CreateQuestionButton'
import { withStyles } from '@material-ui/core/styles'

import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import CloseIcon from '@material-ui/icons/Close'
import IconButton from '@material-ui/core/IconButton'
import MuiDialog from '@material-ui/core/Dialog'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import MuiDialogActions from '@material-ui/core/DialogActions'
import MuiDialogContentText from '@material-ui/core/DialogContentText'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  listRoot: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  dialogRoot: {
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(20),
  },
})

const Dialog = withStyles(theme => ({
  root: {
    fullWidth: true,
  }
}))(MuiDialog)

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props
  return (
    <MuiDialogTitle disableTypography className={classes.dialogRoot} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

const DialogContentText = withStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  }
}))(MuiDialogContentText)

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions)

export default function CreateQuestionDialog(props) {
  const classes = withStyles(styles)

  const [open, setOpen] = React.useState(false)

  const [description, setDescription] = React.useState('') 

  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div>
      <Fab className={classes.fab} color="primary" aria-label="add" size="large" onClick={handleClickOpen}>
        <AddIcon />
      </Fab>

      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Question
        </DialogTitle>
        <DialogContentText>
          <TextField
            autoFocus
            multiline
            margin="dense"
            id="name"
            label="Description"
            value={description}
            onChange={event => setDescription(event.target.value)}
            placeholder="Type in a question description"
            type="text"
            fullWidth
          />
        </DialogContentText>
        <DialogActions>
          <CreateQuestionButton input={{description}}  onClick={handleClose} />
        </DialogActions>
      </Dialog>
    </div>
  )
}



