import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
})

export default function AnswerList(props) {
  const classes = withStyles(styles)
  const answers = props.answers

  return (
    <List className={classes.root}>
      {answers.map(answer => {
        //CHANGE FOR INDEX(HOW TO FIND INDEX OF ANSWER??)
        const labelId = `checkbox-list-label-${answer.id}`

        return (
          <Typography gutterBottom>
            <ListItem key={answer.id} role={undefined} dense button>
              <ListItemText id={labelId} primary={`${answer.description}`} />
            </ListItem>
          </Typography>
        )
      })}
    </List>
  )
}