import React from 'react'
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const CREATEANSWER_MUTATION = gql`
  mutation CreateAnswerMutation($input: AnswerCreateInput!) {
    createAnswer(input: $input) {
      id
      description
    }
  }
`

function CreateAnswer(props) {
  const [createAnswer, { data }] = useMutation(CREATEANSWER_MUTATION)


  console.log(props.input.questionId)

  return (
    <Button
      onClick={e => {
        e.preventDefault()
        createAnswer({
          variables: {
            input: {
              description: props.input.description,
              belongsTo: props.input.questionId,
              isCorrect: false
            }
          }
        })
        props.onClick()
      }}
      color="primary"
    >  
      Create Answer
    </Button>
  ) 
  
}

export default CreateAnswer
