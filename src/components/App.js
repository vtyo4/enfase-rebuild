import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import QuestionList from './question/queries/QuestionList'
import '../styles/App.css'

class App extends Component {
  render() {
    return (
      <div className="center w85">
        <div className="ph3 pv1 background-gray">
          <Switch>
            <Route exact path="/" component={QuestionList} />
          </Switch>
        </div>
      </div>
    )
  }
}

export default App
