function createQuestion(parent, args, context)  {
  console.log("oi", args)

  return context.prisma.createQuestion({
    description: args.input.description
  })
}

function updateQuestion(parent, args, context) {
  return context.prisma.updateQuestion({
    where: {
      id: args.input.id
    },

    data: {
      description: args.input.description
    }
  })
}

function deleteQuestion(parent, args, context) {
  return context.prisma.deleteQuestion({
    id: args.id
  })
}

function createAnswer(parent, args, context) {
  return context.prisma.createAnswer({
    description: args.input.description,
    belongsTo:  { connect: { id: args.input.belongsTo } },
    isCorrect: args.input.isCorrect
  })
}

function updateAnswer(parent, args, context) {
  return context.prisma.updateAnswer({
    where: {
      id: args.input.id
    },

    data: {
      description: args.input.description,
      isCorrect: args.input.isCorrect
    }
  })
}

function deleteAnswer(parent, args, context) {
  return context.prisma.deleteAnswer({ 
    id: args.id
  })
}

module.exports = {
  createQuestion,
  updateQuestion,
  deleteQuestion,
  createAnswer,
  updateAnswer,
  deleteAnswer,
}