function question(parent, args, context, info) {
  return context.prisma.question({ id: args.id })
}

async function questionList(parent, args, context, info) {
  const where = args.filter
  ? {
      OR: [
        { description_contains: args.filter }
      ]
    } 
  : {}

  const questions = await context.prisma.questions({
    where,
    skip: args.skip,
    first: args.first
  })

  const count = await context.prisma.questionsConnection({
    where
  })
  .aggregate()
  .count()


  return {
    questions,
    count
  }
}

module.exports = {
  question,
  questionList,
}