function belongsTo(parent, args, context, info) {
  return context.prisma.answer({ id: parent.id }).belongsTo()
}

module.exports = {
  belongsTo,
}